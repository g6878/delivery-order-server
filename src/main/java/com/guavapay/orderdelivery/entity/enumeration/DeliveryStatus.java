package com.guavapay.orderdelivery.entity.enumeration;

public enum DeliveryStatus {
    ORDERED,
    SHIPPED,
    ON_THE_WAY,
    CANCELLED,
    DELIVERED
}
