package com.guavapay.orderdelivery.entity.enumeration;

public enum CourierStatus {
    FREE,
    ON_DELIVERY
}
