package com.guavapay.orderdelivery.controller;

import com.guavapay.orderdelivery.dto.parcel.ParcelDto;
import com.guavapay.orderdelivery.service.ParcelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/parcels")
public class ParcelController {

    private final ParcelService parcelService;

    @PostMapping
    public void createParcel(@RequestBody ParcelDto parcelDto) {
        parcelService.createParcel(parcelDto);
    }

    @GetMapping
    public Page<ParcelDto> getAllUserParcels(Pageable pageable) {
        return parcelService.currentUserParcels(pageable);
    }
}
