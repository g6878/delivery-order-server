package com.guavapay.orderdelivery.controller;

import com.guavapay.orderdelivery.dto.courier.CourierDto;
import com.guavapay.orderdelivery.dto.courier.CourierRequest;
import com.guavapay.orderdelivery.service.CourierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/couriers")
public class CourierController {

    private final CourierService courierService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public Page<CourierDto> couriers(Pageable pageable){
        return courierService.couriers(pageable);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public void createCourier(@RequestBody CourierRequest courierRequest){
        courierService.createCourier(courierRequest);
    }
}
