package com.guavapay.orderdelivery.controller;

import com.guavapay.orderdelivery.dto.delivery.DeliveryCoordinatesDto;
import com.guavapay.orderdelivery.dto.delivery.DeliveryDto;
import com.guavapay.orderdelivery.dto.delivery.DeliveryRequest;
import com.guavapay.orderdelivery.entity.enumeration.DeliveryStatus;
import com.guavapay.orderdelivery.service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/delivery-orders")
public class DeliveryController {

    private final DeliveryService deliveryService;

    @PostMapping
    public void createDelivery(@RequestBody DeliveryRequest request) {
        deliveryService.createDelivery(request);
    }

    @PutMapping("/{id}/destination/{destination}")
    public void changeDestination(@PathVariable Long id, @PathVariable String destination) {
        deliveryService.changeDestination(id, destination);
    }

    @PutMapping("/{id}/status/{status}")
    public void changeStatus(@PathVariable Long id, @PathVariable DeliveryStatus status) {
        deliveryService.changeStatus(id, status);
    }

    @PutMapping("/{id}/cancel")
    public void cancelDelivery(@PathVariable Long id) {
        deliveryService.cancelDelivery(id);
    }

    @GetMapping("{id}")
    public DeliveryDto deliveryDetails(@PathVariable Long id) {
        return deliveryService.deliveryDetails(id);
    }

    @GetMapping
    public Page<DeliveryDto> myDeliveries(Pageable pageable) {
        return deliveryService.myDeliveries(pageable);
    }

    @PreAuthorize("hasRole('ROLE_COURIER')")
    @GetMapping("/assigned")
    public Page<DeliveryDto> assignedDeliveries(Pageable pageable) {
        return deliveryService.assignedDeliveries(pageable);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/all")
    public Page<DeliveryDto> allDeliveries(Pageable pageable) {
        return deliveryService.allDeliveries(pageable);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/courier/{courierId}/delivery/{deliveryId}")
    public void assignDelivery(@PathVariable Long courierId, @PathVariable Long deliveryId) {
        deliveryService.assignDelivery(courierId, deliveryId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{id}/coordinates")
    public DeliveryCoordinatesDto deliveryCoordinates(@PathVariable Long id) {
        return deliveryService.deliveryCoordinates(id);
    }

}
