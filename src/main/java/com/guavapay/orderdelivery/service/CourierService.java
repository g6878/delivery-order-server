package com.guavapay.orderdelivery.service;

import com.guavapay.orderdelivery.client.AuthClient;
import com.guavapay.orderdelivery.dto.courier.CourierDto;
import com.guavapay.orderdelivery.dto.courier.CourierRequest;
import com.guavapay.orderdelivery.entity.Courier;
import com.guavapay.orderdelivery.entity.enumeration.CourierStatus;
import com.guavapay.orderdelivery.exceptions.CourierNotFoundException;
import com.guavapay.orderdelivery.repository.CourierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourierService {

    private final CourierRepository courierRepository;
    private final AuthClient authClient;

    public Page<CourierDto> couriers(Pageable pageable) {
        return courierRepository.findAll(pageable).map(this::toCourierDto);
    }

    public void createCourier(CourierRequest courierRequest) {
        authClient.createCourier(courierRequest);

        Courier courier = new Courier()
                .setName(courierRequest.getEmail())
                .setUserEmail(courierRequest.getEmail());

        courierRepository.save(courier);
    }

    public void changeStatus(Long courierId, CourierStatus courierStatus) {
        Courier courier = courierRepository.findById(courierId).orElseThrow(CourierNotFoundException::new);
        courier.setStatus(courierStatus);
        courierRepository.save(courier);
    }

    private CourierDto toCourierDto(Courier courier) {
        return new CourierDto()
                .setName(courier.getName())
                .setStatus(courier.getStatus().toString());
    }
}
