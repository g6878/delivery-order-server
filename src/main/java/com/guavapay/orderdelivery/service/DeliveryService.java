package com.guavapay.orderdelivery.service;

import com.guavapay.orderdelivery.dto.delivery.DeliveryCoordinatesDto;
import com.guavapay.orderdelivery.dto.delivery.DeliveryDto;
import com.guavapay.orderdelivery.dto.delivery.DeliveryRequest;
import com.guavapay.orderdelivery.entity.Courier;
import com.guavapay.orderdelivery.entity.Delivery;
import com.guavapay.orderdelivery.entity.enumeration.CourierStatus;
import com.guavapay.orderdelivery.entity.enumeration.DeliveryStatus;
import com.guavapay.orderdelivery.exceptions.CannotCancelDeliveryException;
import com.guavapay.orderdelivery.exceptions.CannotChangeDestinationException;
import com.guavapay.orderdelivery.exceptions.CannotChangeStatusException;
import com.guavapay.orderdelivery.exceptions.DeliveryNotFoundException;
import com.guavapay.orderdelivery.repository.DeliveryRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final DeliveryRepository deliveryRepository;
    private final ParcelService parcelService;
    private final UserService userService;
    private final CourierService courierService;

    @Transactional
    public void createDelivery(DeliveryRequest deliveryRequest) {
        Delivery delivery = new Delivery()
                .setShippingAddress(deliveryRequest.getShippingAddress())
                .setUserEmail(userService.currentUserEmail())
                .setStatus(DeliveryStatus.ORDERED)
                .setNumber(UUID.randomUUID().toString());

        deliveryRepository.save(delivery);

        deliveryRequest.getParcelIds()
                .forEach(parcelId -> parcelService.addToDelivery(parcelId, delivery.getId()));
    }

    public void changeDestination(Long deliveryId, String destination) {
        Delivery delivery = delivery(deliveryId);

        if (!DeliveryStatus.ORDERED.equals(delivery.getStatus()) ||
            !DeliveryStatus.SHIPPED.equals(delivery.getStatus())) throw new CannotChangeDestinationException();

        delivery.setShippingAddress(destination);
        deliveryRepository.save(delivery);
    }

    public void changeStatus(Long deliveryId, DeliveryStatus status) {
        Delivery delivery = deliveryRepository
                .findById(deliveryId).orElseThrow(DeliveryNotFoundException::new);

        if (DeliveryStatus.CANCELLED.equals(status) || DeliveryStatus.DELIVERED.equals(status)) {
            courierService.changeStatus(delivery.getCourier().getId(), CourierStatus.FREE);
        }

        delivery.setStatus(status);
        deliveryRepository.save(delivery);
    }

    public void cancelDelivery(Long deliveryId) {
        Delivery delivery = delivery(deliveryId);

        if (DeliveryStatus.ON_THE_WAY.equals(delivery.getStatus())) throw new CannotCancelDeliveryException();
        courierService.changeStatus(delivery.getCourier().getId(), CourierStatus.FREE);

        deliveryRepository.delete(delivery);
    }

    public DeliveryDto deliveryDetails(Long deliveryId) {
        Delivery delivery = delivery(deliveryId);

        DeliveryDto deliveryDto = new DeliveryDto();
        deliveryDto
                .setNumber(delivery.getNumber())
                .setShippingAddress(delivery.getShippingAddress())
                .setStatus(delivery.getStatus())
                .setPrice(delivery.getPrice());

        return deliveryDto;
    }

    public DeliveryCoordinatesDto deliveryCoordinates(Long deliveryId) {
        Delivery delivery = delivery(deliveryId);

        DeliveryCoordinatesDto coordinatesDto = new DeliveryCoordinatesDto();
        coordinatesDto
                .setLatitude(delivery.getLatitude())
                .setLongitude(delivery.getLongitude());

        return coordinatesDto;
    }

    public Page<DeliveryDto> myDeliveries(Pageable pageable) {
        String userEmail = userService.currentUserEmail();
        Page<Delivery> deliveries = deliveryRepository.findByUserEmail(userEmail, pageable);

        return deliveries.map(this::toDeliveryDto);
    }

    public Page<DeliveryDto> allDeliveries(Pageable pageable){
        return deliveryRepository.findAll(pageable)
                .map(this::toDeliveryDto);
    }

    public Page<DeliveryDto> assignedDeliveries(Pageable pageable) {
        String courierEmail = userService.currentUserEmail();

        Page<Delivery> deliveries = deliveryRepository.findByCourierUserEmail(courierEmail, pageable);

        return deliveries.map(this::toDeliveryDto);
    }

    public void assignDelivery(Long courierId, Long deliveryId) {
        Delivery delivery = delivery(deliveryId);

        courierService.changeStatus(courierId, CourierStatus.ON_DELIVERY);

        delivery.setCourier(new Courier().setId(courierId));

        deliveryRepository.save(delivery);
    }

    private DeliveryDto toDeliveryDto(Delivery delivery) {
        return new DeliveryDto()
                .setPrice(delivery.getPrice())
                .setStatus(delivery.getStatus())
                .setNumber(delivery.getNumber())
                .setShippingAddress(delivery.getShippingAddress());
    }

    private Delivery delivery(Long deliveryId) {
        return deliveryRepository.findById(deliveryId)
                .orElseThrow(DeliveryNotFoundException::new);
    }
}
