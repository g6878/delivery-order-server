package com.guavapay.orderdelivery.service;

import com.guavapay.orderdelivery.dto.parcel.ParcelDto;
import com.guavapay.orderdelivery.entity.Delivery;
import com.guavapay.orderdelivery.entity.Parcel;
import com.guavapay.orderdelivery.exceptions.ParcelNotFoundException;
import com.guavapay.orderdelivery.repository.ParcelRepository;
import java.util.Random;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ParcelService {

    private final ParcelRepository parcelRepository;
    private final UserService userService;

    public void createParcel(ParcelDto parcelDto) {
        Parcel parcel = new Parcel()
                .setDescription(parcelDto.getDescription())
                .setTitle(parcelDto.getTitle())
                .setUserEmail(userService.currentUserEmail())
                .setWeight(new Random().nextInt(10) * 0.3);

        parcelRepository.save(parcel);
    }

    public Page<ParcelDto> currentUserParcels(Pageable pageable) {
        Page<Parcel> parcels = parcelRepository.findByUserEmail(userService.currentUserEmail(), pageable);

        return parcels.map(this::toParcelDto);
    }

    public void addToDelivery(Long parcelId, Long deliveryId) {
        Parcel parcel = parcelRepository.findById(parcelId).orElseThrow(ParcelNotFoundException::new);

        if (parcel.getDelivery() == null) {
            parcel.setDelivery(new Delivery().setId(deliveryId));
            parcelRepository.save(parcel);
        }
    }

    private ParcelDto toParcelDto(Parcel parcel) {
        return new ParcelDto()
                .setTitle(parcel.getTitle())
                .setDescription(parcel.getDescription());
    }
}
