package com.guavapay.orderdelivery.client;

import com.guavapay.orderdelivery.dto.courier.CourierRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "zuul-gateway")
public interface AuthClient {

    @PostMapping(value = "/auth-server/account/courier")
    void createCourier(@RequestBody CourierRequest courierRequest);
}
