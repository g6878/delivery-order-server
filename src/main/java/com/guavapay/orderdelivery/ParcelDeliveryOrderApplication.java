package com.guavapay.orderdelivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableEurekaClient
@EnableFeignClients
@EnableResourceServer
@SpringBootApplication
public class ParcelDeliveryOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParcelDeliveryOrderApplication.class, args);
    }
}
