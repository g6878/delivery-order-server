package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.DELIVERY_NOT_FOUND;

//todo add id of delivery as info to exception message
public class DeliveryNotFoundException extends RuntimeException {

    public DeliveryNotFoundException() {
        super(DELIVERY_NOT_FOUND);
    }
}
