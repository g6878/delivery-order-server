package com.guavapay.orderdelivery.exceptions;

public interface ErrorMessage {
    String DELIVERY_NOT_FOUND = "Delivery not found";
    String PARCEL_NOT_FOUND = "Parcel not found";
    String CANNOT_CHANGE_STATUS = "Cannot change status";
    String CANNOT_CANCEL_DELIVERY = "Cannot cancel delivery";
    String CANNOT_CHANGE_DESTINATION = "Cannot change destination";
    String COURIER_NOT_FOUND = "Cannot change destination";
}
