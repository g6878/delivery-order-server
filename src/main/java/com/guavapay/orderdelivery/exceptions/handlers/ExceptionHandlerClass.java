package com.guavapay.orderdelivery.exceptions.handlers;

import com.guavapay.orderdelivery.exceptions.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
public class ExceptionHandlerClass extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DeliveryNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(DeliveryNotFoundException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(ParcelNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(ParcelNotFoundException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(CannotChangeStatusException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(CannotChangeStatusException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(CannotCancelDeliveryException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(CannotCancelDeliveryException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(CannotChangeDestinationException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(CannotChangeDestinationException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(CourierNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handle(CourierNotFoundException exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<ErrorResponse> handle(HttpStatusCodeException exception) {
        logger.error(exception.getMessage());
        return ResponseEntity
                .status(exception.getStatusCode())
                .body(ErrorResponse.ofResponseEntity(exception.getResponseBodyAsString()));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponse handle(Exception exception) {
        logger.error(exception.getMessage());
        return ErrorResponse.of(exception);
    }

}
