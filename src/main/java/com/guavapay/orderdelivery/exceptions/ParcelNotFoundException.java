package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.PARCEL_NOT_FOUND;

public class ParcelNotFoundException extends RuntimeException {
    public ParcelNotFoundException() {
        super(PARCEL_NOT_FOUND);
    }
}
