package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.CANNOT_CANCEL_DELIVERY;

public class CannotCancelDeliveryException extends RuntimeException {
    public CannotCancelDeliveryException() {
        super(CANNOT_CANCEL_DELIVERY);
    }
}
