package com.guavapay.orderdelivery.exceptions;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorResponse implements Serializable {
    private final String message;


    public static ErrorResponse of(Exception e) {
        return new ErrorResponse(e.getMessage());
    }

    public static ErrorResponse of(String errorMessage) {
        return new ErrorResponse(errorMessage);
    }

    public static ErrorResponse ofResponseEntity(String errorMessage) {
        return new ErrorResponse(
                errorMessage.contains(":") ?
                        errorMessage.split(":")[1].replace("\"","").replace("}","") :
                        errorMessage
        );
    }
}
