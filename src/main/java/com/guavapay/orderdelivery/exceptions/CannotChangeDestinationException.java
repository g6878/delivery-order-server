package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.CANNOT_CHANGE_DESTINATION;

public class CannotChangeDestinationException extends RuntimeException {
    public CannotChangeDestinationException() {
        super(CANNOT_CHANGE_DESTINATION);
    }
}
