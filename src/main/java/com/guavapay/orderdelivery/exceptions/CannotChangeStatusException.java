package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.CANNOT_CHANGE_STATUS;

public class CannotChangeStatusException extends RuntimeException {
    public CannotChangeStatusException() {
        super(CANNOT_CHANGE_STATUS);
    }
}
