package com.guavapay.orderdelivery.exceptions;

import static com.guavapay.orderdelivery.exceptions.ErrorMessage.COURIER_NOT_FOUND;

public class CourierNotFoundException extends RuntimeException {
    public CourierNotFoundException() {
        super(COURIER_NOT_FOUND);
    }
}
