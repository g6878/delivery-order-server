package com.guavapay.orderdelivery.dto.parcel;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class ParcelDto {
    private String title;
    private String description;
}
