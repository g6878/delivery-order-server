package com.guavapay.orderdelivery.dto.courier;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourierDto {
    private String name;
    private String status;
}
