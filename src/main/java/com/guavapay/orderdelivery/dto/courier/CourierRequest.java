package com.guavapay.orderdelivery.dto.courier;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourierRequest {
    private String email;
    private char[] password;
    private char[] confirmPassword;
}
