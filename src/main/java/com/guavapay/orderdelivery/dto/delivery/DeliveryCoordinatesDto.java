package com.guavapay.orderdelivery.dto.delivery;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryCoordinatesDto {
    private Double latitude;
    private Double longitude;
}
