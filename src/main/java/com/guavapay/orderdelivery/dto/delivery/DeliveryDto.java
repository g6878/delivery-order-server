package com.guavapay.orderdelivery.dto.delivery;

import com.guavapay.orderdelivery.entity.enumeration.DeliveryStatus;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Setter
@Getter
public class DeliveryDto {
    private String number;
    private String shippingAddress;
    private DeliveryStatus status;
    private BigDecimal price;
}
