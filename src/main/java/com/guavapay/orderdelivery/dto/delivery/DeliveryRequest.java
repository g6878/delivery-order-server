package com.guavapay.orderdelivery.dto.delivery;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class DeliveryRequest {
    private String shippingAddress;
    private Set<Long> parcelIds;
}
