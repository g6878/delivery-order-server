package com.guavapay.orderdelivery.repository;

import com.guavapay.orderdelivery.entity.Delivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    Page<Delivery> findByUserEmail(String email, Pageable pageable);

    Page<Delivery> findByCourierUserEmail(String courierEmail, Pageable pageable);
}
