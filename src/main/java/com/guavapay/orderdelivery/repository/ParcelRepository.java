package com.guavapay.orderdelivery.repository;

import com.guavapay.orderdelivery.entity.Parcel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParcelRepository extends JpaRepository<Parcel, Long> {

    Page<Parcel> findByUserEmail(String email, Pageable pageable);

}
