package com.guavapay.orderdelivery.repository;

import com.guavapay.orderdelivery.entity.Courier;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CourierRepository extends JpaRepository<Courier, Long> {

}
