package com.guavapay.orderdelivery.service;

import com.guavapay.orderdelivery.entity.Delivery;
import com.guavapay.orderdelivery.entity.enumeration.DeliveryStatus;
import com.guavapay.orderdelivery.exceptions.CannotCancelDeliveryException;
import com.guavapay.orderdelivery.exceptions.CannotChangeDestinationException;
import com.guavapay.orderdelivery.exceptions.CannotChangeStatusException;
import com.guavapay.orderdelivery.exceptions.DeliveryNotFoundException;
import com.guavapay.orderdelivery.repository.DeliveryRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;

@ExtendWith(MockitoExtension.class)
class DeliveryServiceTest {

    @Mock
    DeliveryRepository deliveryRepository;

    @InjectMocks
    DeliveryService deliveryService;

    @Test
    void checkIfStatusNotEqualsChangeDestinationThrowsCannotChangeDestinationException() {
        Delivery delivery = new Delivery().setStatus(DeliveryStatus.ON_THE_WAY);

        Mockito.when(deliveryRepository.findById(anyLong())).thenReturn(Optional.of(delivery));

        assertThrows(CannotChangeDestinationException.class, () -> deliveryService.changeDestination(1L, "test"));
    }

    @Test
    void checkIfDeliveryNotFoundChangeStatusThrowsDeliveryNotFoundException() {
        Mockito.when(deliveryRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(DeliveryNotFoundException.class, () -> deliveryService.changeStatus(1L, DeliveryStatus.DELIVERED));
    }

    @Test
    void checkIfStatusEqualsCancelDeliveryThrowsCannotCancelDeliveryException() {
        Delivery delivery = new Delivery().setStatus(DeliveryStatus.ON_THE_WAY);

        Mockito.when(deliveryRepository.findById(anyLong())).thenReturn(Optional.of(delivery));

        assertThrows(CannotCancelDeliveryException.class, () -> deliveryService.cancelDelivery(1L));
    }

}