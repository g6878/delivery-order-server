FROM openjdk:11
VOLUME /tmp
EXPOSE 8761
ADD build/libs/*.jar order-delivery.jar
ENV JAVA_OPTS = "-Xmx1536m"
ENTRYPOINT ["sh","-c", "java -Xmx2200m -jar order-delivery.jar"]